__authors__ = "Alessio Bogon, Giulia Preti, Davide Spadini"

from collections import namedtuple
from Queue import Queue, Empty
import logging

import failure_detector
import comms
import sys

CMessage = namedtuple("CMessage", ["type", "round", "value", "sender"])


class Consensus(object):
    """Process p_i"""
    def __init__(
            self,
            pid,
            routing_table,
            consensus_receiver,
            fd,
            value,
            missing_assumption=None
            ):
        self.pid = pid
        self.round = 0
        self.est = value
        self.n = len(routing_table)

        self.decided = False
        self.stop = False

        self.fd = fd                                    # Failure detector
        self.routing_table = routing_table

        # Save the assumption is missing in the current experiment
        self.missing_assumption = missing_assumption

        # Queue of received messages
        self.receive_queue = Queue()
        self.consensus_receiver = consensus_receiver
        self.consensus_receiver.register(self.receive_queue.put)

        # Check if we need to simulate a non-perfect channel.
        # i.e. do not listen for "DECIDE" messages
        if not (self.missing_assumption == "perfect_channel" and self.pid == 0):
            self.consensus_receiver.register(self.check_decide)

        # Check if we need to simulate a channel with increasing delay
        if missing_assumption == "partially_async":
            self.increment_delay = True
        else:
            self.increment_delay = False

    def b_broadcast(self, message):
        for pid, (ip, port) in self.routing_table.iteritems():
            # If we need to simualte a ring-based topology (i.e. not fully
            # connected ) then send messages only to the next node
            if pid != self.pid+1 and self.missing_assumption == "fully_connected":
                continue
            comms.send(ip, port, message, reliable=True,
                       increment_delay=self.increment_delay)

    def decide(self, value):
        print "! ", self.pid, " decided for ", value

    def execution_algorithm(self):
        while not self.stop:
            print "Begin of round %s" % self.round

            # coordinator of the current round
            c = self.round % self.n
            self.round += 1

            # Simulate the coordinator crash in case we want to test a dummy
            # failure detector
            if self.pid == c and self.missing_assumption == "failure_detector":
                sys.exit(0)

            # Simulate a crash of the n/2 lower nodes in case we want to test
            # the bounded maximum failures property
            if self.missing_assumption == "max_failures" and self.pid <= self.n/2:
                sys.exit(0)

            # Simulate byzantine behavior: Node 0 sends to everyone a different
            # "DECIDE" message, so that each of the nodes will decide a
            # different value
            if self.missing_assumption == "not_byzantine" and self.pid == 0:
                for i in xrange(self.n):
                    message = CMessage(
                            "DECIDE",
                            round=None,
                            value=i,
                            sender=None,
                        )
                    comms.send(
                        self.routing_table[i][0],
                        self.routing_table[i][1],
                        message,
                        reliable=True
                    )

            # PHASE 1
            if self.pid == c:
                print "I'm the coordinator (%s), sending PHASE1" % self.pid
                m1 = CMessage(
                    "PHASE1",
                    round=self.round,
                    value=self.est,
                    sender=self.pid,
                )
                self.b_broadcast(m1)

            aux = None

            phase1_received = False
            while not self.fd.suspects(c) and not phase1_received:
                try:
                    m = self.receive_queue.get(1)
                    if m.type == "PHASE1" and m.round == self.round:
                        print "Received PHASE1, saving to aux = ", m.value
                        aux = m.value
                        phase1_received = True
                    else:
                        self.receive_queue.put(m)
                except Empty:
                    pass

            # PHASE 2
            # broadcast to everybody phase 2
            m2 = CMessage(
                "PHASE2",
                round=self.round,
                value=aux,
                sender=self.pid,
            )
            self.b_broadcast(m2)

            rec = set()
            proc = set()

            while len(proc) <= self.n / 2:
                m = self.receive_queue.get()
                must_drop_phase2 = self.missing_assumption == "perfect_channel" and self.pid == 0
                if m.type == "PHASE2" and m.round == self.round and not must_drop_phase2:
                    rec.add(m.value)
                    proc.add(m.sender)
                else:
                    self.receive_queue.put(m)

            if len(rec) == 1:
                v = rec.pop()
                if v is not None:
                    self.est = v

                    message = CMessage(
                        "DECIDE",
                        round=None,
                        value=self.est,
                        sender=None,
                    )
                    self.b_broadcast(message)

                    self.stop = True
            elif len(rec) > 1:
                for v in rec:
                    if v is not None:
                        self.est = v

    def check_decide(self, message):
        # print "{}: received message {}".format(self.pid, message)
        if message.type != "DECIDE":
            return
        if not self.decided:
            self.decided = True
            self.b_broadcast(message)
            self.decide(message.value)


if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)

    pid = int(sys.argv[1])
    n = int(sys.argv[2])
    try:
        missing_assumption = sys.argv[3]
    except IndexError:
        missing_assumption = None

    fd_routing_table = {i: ("localhost", 10000 + i) for i in xrange(n)}

    fd_receiver = comms.MessageReceiver(
        host=fd_routing_table[pid][0],
        port=fd_routing_table[pid][1],
    )

    # Choose the failure detector according to the parameters given
    if missing_assumption == "failure_detector":
        # dummy fd
        fd = failure_detector.DummyFailureDetector()
    else:
        # Real fd
        fd = failure_detector.FailureDetector(
            pid, fd_routing_table, fd_receiver, interval=1,
            missing_assumption=missing_assumption)

    consensus_routing_table = {i: ("localhost", 20000 + i) for i in xrange(n)}
    consensus_receiver = comms.MessageReceiver(
        host=consensus_routing_table[pid][0],
        port=consensus_routing_table[pid][1],
    )

    value = pid
    consensus = Consensus(
        pid,
        consensus_routing_table,
        consensus_receiver,
        fd,
        value,
        missing_assumption
    )
    raw_input("Enter to start algorithm\n")
    consensus.execution_algorithm()

    raw_input("enter to exit")
