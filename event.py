__authors__ = "Alessio Bogon, Giulia Preti, Davide Spadini"

import threading


class EventEmitter(object):
    def __init__(self):
        self._cond = threading.Condition()

    def wait(self):
        with self._cond:
            self._cond.wait()

    def emit(self):
        with self._cond:
            self._cond.notify_all()


class EventRegister(object):
    def __init__(self):
        self.handlers = set()

    def register(self, handler):
        self.handlers.add(handler)
        return self

    def unregister(self, handler):
        try:
            self.handlers.remove(handler)
        except:
            raise ValueError("Handler is not handling this event, so " +
                             "cannot unhandle it.")
        return self

    def trigger(self, *args, **kwargs):
        for handler in self.handlers:
            t = threading.Thread(target=handler, args=args, kwargs=kwargs)
            # handler(*args, **kargs)
            t.daemon = True
            t.start()

    def __len__(self):
        return len(self.handlers)

    __iadd__ = register
    __isub__ = unregister
    __call__ = trigger
