__authors__ = "Alessio Bogon, Giulia Preti, Davide Spadini"

import SocketServer
import threading
import pickle
import socket

import event
import time

acc_send_delay = 0


class MessageReceiver(event.EventRegister):
    def __init__(self, host="0.0.0.0", port=None):
        event.EventRegister.__init__(self)

        class ThreadedTCPRequestHandler(SocketServer.StreamRequestHandler):
            def handle(self_):
                try:
                    message = pickle.load(self_.rfile)
                    self.trigger(message)
                except Exception:
                    print "Received invalid message"

        self.server = SocketServer.ThreadingTCPServer(
            (host, port), ThreadedTCPRequestHandler)

        self.server_daemon = threading.Thread(target=self.server.serve_forever)
        self.server_daemon.daemon = True
        self.server_daemon.start()


def send(host, port, message, reliable=True, increment_delay=False):
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    def do_stuff():
        global acc_send_delay
        time.sleep(acc_send_delay)
        if increment_delay:
            acc_send_delay += 2
        data = pickle.dumps(message)

        # Try resending until success
        sent = False
        while not sent:
            try:
                sock.connect((host, port))
                sock.sendall(data)
                sent = True
            except:
                # If we don't want reliable comms, don't resend
                if not reliable:
                    sent = True

    # Send the data in a different thread
    t = threading.Thread(target=do_stuff)
    t.daemon = True
    t.start()
