__authors__ = "Alessio Bogon, Giulia Preti, Davide Spadini"

from collections import namedtuple
from threading import Timer
import logging

import event
import comms


FDMessage = namedtuple("FDMessage", ["type", "sender"])


def timeout_thread(*args, **kwargs):
    t = Timer(*args, **kwargs)
    t.daemon = True
    t.start()
    return t


# The dummy failure detector just suspects nobody
class DummyFailureDetector(object):
    def suspects(self, pid):
        return False


class FailureDetector(object):
    def __init__(self, id, routing_table, message_receiver,
            interval=5, missing_assumption=None):
        self.l = logging.getLogger('FailureDetector')
        self.id = id
        self.n = len(routing_table)
        self.trusted = 0
        self.interval = interval

        self.suspect_emitter = event.EventRegister()

        self.routing_table = routing_table
        self.message_receiver = message_receiver
        self.message_receiver.register(self.b_deliver)

        self._new_leader_event = event.EventEmitter()

        if missing_assumption == "partially_async":
            self.increment_delay = True
        else:
            self.increment_delay = False

        self.delta = [1] * self.n

        self.timer_send = timeout_thread(interval=1, function=self.task1)
        self.timeout_leader = timeout_thread(
            interval=self.delta[self.trusted],
            function=self.leader_not_available,
            )

    def suspects(self, pid):
        return pid != self.trusted

    def suspects_wait(self, pid):
        while pid == self.trusted:
            self._new_leader_event.wait()
        return True

    def b_send(self, pid, message):
        ip, port = self.routing_table[pid]
        comms.send(ip, port, message, reliable=False,
            increment_delay=self.increment_delay)

    # Task 1
    def task1(self):
        self.l.info("FD {}: task1. trusted is: {}".format(self.id,
            self.trusted))

        if self.trusted == self.id:
            for p in xrange(self.id + 1, self.n):
                self.l.info("FD {}: sending i-am-the-leader to {}".format(
                    self.id, p))
                self.b_send(p, FDMessage("i-am-the-leader", self.id))

        self.timer_send = timeout_thread(
            interval=self.interval,
            function=self.task1
        )

    def b_deliver(self, message):
        j = message.sender

        self.timeout_leader.cancel()
        self.l.info("FD {}: received ping from {}".format(self.id,
            message.sender))

        # Task 3
        if j < self.trusted:
            self.trusted = j
            self.delta[j] += 1
            self.l.info("""FD {}: message received from j({}) < trusted({}). j is the new trusted.
trusted: {}, delta: {}""".format(
                self.id, j, self.trusted, self.trusted,
                self.delta[self.trusted])
            )
            self._new_leader_event.emit()
            self.suspect_emitter.trigger(self.trusted)

        self.timeout_leader = timeout_thread(
            interval=self.delta[self.trusted],
            function=self.leader_not_available,
        )

    # Executed when no message was received from the leader
    # within the delta bound
    # Task 2
    def leader_not_available(self):
        self.l.info("FD {}: leader_not_available (delta was {})".format(
            self.id, self.delta[self.trusted]))
        if self.trusted < self.id:
            self.trusted += 1
            self.l.info(
                "FD {}: Incremented trusted. trusted: {}, timeout: {}".format(
                    self.id, self.trusted, self.delta[self.trusted]))
            self._new_leader_event.emit()
            self.suspect_emitter.trigger(self.trusted)

            self.timeout_leader.cancel()
            self.timeout_leader = timeout_thread(
                interval=self.delta[self.trusted],
                function=self.leader_not_available,
            )

"""
if __name__ == "__main__":
    import sys
    # logging.basicConfig(level=logging.DEBUG)
    pid = int(sys.argv[1])
    n = int(sys.argv[2])

    routing_table = {i: ("localhost", 10000 + i) for i in xrange(n)}

    fd_receiver = comms.MessageReceiver(
        host=routing_table[pid][0],
        port=routing_table[pid][1],
    )
    fd = FailureDetector(pid, routing_table, fd_receiver, interval=1)

    while True:
        import time
        time.sleep(1)
        try:
            who = input("Oracle suspect for whom?")
            s = fd.suspects(who)
            print "Oracle says: ", s
            if s:
                continue

            yn = raw_input(
                "Do you want to wait for him become a suspect? [yn]")
            if yn == 'y':
                fd.suspects_wait(who)
                print "SUPSPECTED ", who
        except:
            sys.exit(1)
"""
